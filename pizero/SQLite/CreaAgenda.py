import sqlite3

# Creamos la conexión a la base de datos
conn = sqlite3.connect("agenda.db")

# Creamos la tabla
cur = conn.cursor()
cur.execute("CREATE TABLE agenda (id INTEGER PRIMARY KEY, nombre TEXT, fecha DATE, hora TIME, descripcion TEXT)")

# Insertamos los datos
for i in range(15):
    nombre = "Contacto" + str(i)
    fecha = "2023-10-23"
    hora = "12:00:00"
    descripcion = "Descripción del contacto" + str(i)
    cur.execute("INSERT INTO agenda (nombre, fecha, hora, descripcion) VALUES (?, ?, ?, ?)", (nombre, fecha, hora, descripcion))

# Guardamos los cambios
conn.commit()

# Cerramos la conexión
conn.close()

