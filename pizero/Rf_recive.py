import rpi_rf

# Define el pin GPIO que se utilizará para recibir la señal
pin = 17

# Define el modo de recepción
mode = rpi_rf.MODE_OOK

# Inicializa el módulo de RF GPIO
rpi_rf.init(pin, mode)

# Bucle infinito para recibir señales
while True:

    # Recibe una señal
    code = rpi_rf.receive_signal()

    # Si se recibió una señal, imprime el código
    if code:
        print(code)
