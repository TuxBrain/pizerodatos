#!/usr/bin/python3
import RPi.GPIO as GPIO
import time

# Configura el modo de pines (BCM o BOARD)
GPIO.setmode(GPIO.BCM)

# Define el número del pin GPIO al que está conectado el LED
pin_led = 26

# Configura el pin del LED como salida
GPIO.setup(pin_led, GPIO.OUT)
print("Corriendo el programa")
try:
    while True:
        # Enciende el LED
        GPIO.output(pin_led, GPIO.HIGH)
        time.sleep(0.5)  # Espera 1 segundo

        # Apaga el LED
        GPIO.output(pin_led, GPIO.LOW)
        time.sleep(0.5)  # Espera 1 segundo

except KeyboardInterrupt:
    pass

# Limpia los pines GPIO
GPIO.cleanup()
