import RPi.GPIO as GPIO
import time

# Configuramos el GPIO como GPIO.BCM
GPIO.setmode(GPIO.BCM)

# Definimos el pin del botón
pin_boton = 26

# Configuramos el pin como entrada con PULL_UP
GPIO.setup(pin_boton, GPIO.IN, pull_up_down=GPIO.PUD_UP)
# Definimos el pin del LED
pin_led = 4
# Configuramos el pin del LED como salida
GPIO.setup(pin_led, GPIO.OUT)


cont=0




print("Corriendo el programa .....")
# Bucle infinito

try:
	while True:
    	#Leemos el estado del botón
		estado_boton = GPIO.input(pin_boton)
	#Si el botón está presionado, imprimimos un mensaje
		if estado_boton == GPIO.LOW:
			cont=cont+1
			print("El botón está presionado :",cont)
			GPIO.output(pin_led, GPIO.LOW)
		else:
			 GPIO.output(pin_led, GPIO.HIGH)
   		 #Esperamos un segundo
		time.sleep(0.5)
except KeyboardInterrupt:
		pass
# Limpia los pines GPIO
		GPIO.cleanup()
